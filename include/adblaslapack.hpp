#ifndef ADBLASLAPACK_HPP
#define ADBLASLAPACK_HPP
#include <codi.hpp>

typedef long int logical;
typedef int integer;
typedef codi::RealForwardGen<double> t1s;
typedef codi::RealReverseGen<double> a1s;

#ifdef NO_UNDERSCORE
#define DGEMM dgemm
#define DAXPY daxpy
#define XERBLA xerbla
#define LSAME lsame
#else
#define DGEMM dgemm_
#define DAXPY daxpy_
#define XERBLA xerbla_
#define LSAME lsame_
#endif
void DGEMM(char *transa, char *transb, integer *m, integer *
          n, integer *k, t1s *alpha, t1s *a, integer *lda,
          t1s *b, integer *ldb, t1s *beta, t1s *c__, 
          integer *ldc);
  
void DGEMM(char *transa, char *transb, integer *m, integer *
          n, integer *k, a1s *alpha, a1s *a, integer *lda,
          a1s *b, integer *ldb, a1s *beta, a1s *c__, 
          integer *ldc);       
  
void DAXPY(integer *n, t1s *da, t1s *dx,
          integer *incx, t1s *dy, integer *incy);
  
void DAXPY(integer *n, a1s *da, a1s *dx,
          integer *incx, a1s *dy, integer *incy);

logical LSAME(char *, const char *);
int XERBLA(const char *, integer *);
#endif

