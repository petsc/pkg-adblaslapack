CXX = clang++
CFLAGS = -g -O0
CODI_DIR = $(HOME)/git/CoDiPack
CFLAGS += -I../include -std=c++11 -I$(CODI_DIR)/include
# set Fortran default underscore mangling of subroutines 
#CFLAGS += -DNO_UNDERSCORE
AR = ar
AROPT = rcs
